from django.contrib.auth.models import User, Group
from rest_framework import serializers
from sp_todos.models import Todo
from sp_notes.models import Note
from sp_snippets.models import Snippet


class UserSerializer(serializers.HyperlinkedModelSerializer):
    notes = serializers.HyperlinkedRelatedField(
        queryset=Note.objects.all(),
        view_name='note-detail',
        many=True)
    snippets = serializers.HyperlinkedRelatedField(
        queryset=Snippet.objects.all(),
        view_name='snippet-detail',
        many=True)
    todos = serializers.HyperlinkedRelatedField(
        queryset=Todo.objects.all(),
        view_name='todo-detail',
        many=True)

    class Meta:
        model = User
        fields = ('url', 'username', 'notes', 'snippets', 'todos')


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group