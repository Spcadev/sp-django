from wagtail.wagtailcore import blocks


class InfoBlock(blocks.StructBlock):
    message = blocks.TextBlock(
        required=True,
        help_text='An informative message, like a tip or suggestion.  Powered by Markdown',
    )

    class Meta:
        template = 'sp_blog/blocks/info.html'
        icon = 'placeholder'


class WarningBlock(blocks.StructBlock):
    message = blocks.TextBlock(
        required=True,
        help_text='A strong warning of caution.  Powered by Markdown',
    )

    class Meta:
        template = 'sp_blog/blocks/warning.html'
        icon = 'placeholder'



class DangerBlock(blocks.StructBlock):
    message = blocks.TextBlock(
        required=True,
        help_text='A dire alert; something catastrophic could happen.  Powered by Markdown',
    )

    class Meta:
        template = 'sp_blog/blocks/danger.html'
        icon = 'placeholder'



class SnippetBlock(blocks.StructBlock):
    code = blocks.TextBlock(
        required=True,
        help_text='Enter the code snippet here.'
    )
    file = blocks.CharBlock(
        required=False,
        help_text='If this code belongs in a specific file (eg: urls.py) enter that file name here.'
    )
    code_type = blocks.CharBlock(
        required=False,
        help_text='Enter the type of code (eg: Python) to help with syntax colouring.'
    )
    source_url = blocks.URLBlock(
        required=False,
        min_length=0,
        max_length=2048,
        help_text='If this code belongs to someone else, please link a source url.'
    )

    class Meta:
        template = 'sp_blog/blocks/code_snippet.html'
        icon = 'code'


class ChecklistBlock(blocks.StructBlock):
    item = blocks.CharBlock(
        required=True,
        help_text='Enter a list item.'
    )
    checked = blocks.BooleanBlock(
        required=False,
        help_text='Has this item been completed?'
    )

    class Meta:
        template = 'sp_blog/blocks/check_list.html'
        icon = 'tick'


class MarkdownBlock(blocks.StructBlock):
    body = blocks.TextBlock(
        required=True,
        help_text='Markdown text editor.'
    )

    class Meta:
        template = 'sp_blog/blocks/markdown.html'
        icon = 'edit'
