from django.contrib import admin

from .models import BlogPage, Series


class SeriesAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "entries":
            kwargs["queryset"] = BlogPage.objects.live().descendant_of(self)
        return super(SeriesAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(Series, SeriesAdmin)