import datetime

from django.db import models


class LiveManager(models.Manager):
    def get_queryset(self):
        return super(LiveManager, self).get_queryset()\
            .exclude(date__gt=datetime.datetime.now)\
            .filter(live=True, date__lte=datetime.datetime.now).order_by('-date')


class SeriesEntriesManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.filter_dict = dict(live=True, date__lte=datetime.datetime.now)
        super(SeriesEntriesManager, self).__init__(*args, **kwargs)

    def get_query_set(self):
        return super(SeriesEntriesManager, self).get_query_set()\
                                                .exclude(date__gt=datetime.datetime.now())\
                                                .filter(path__gte=self.path)\
                                                .order_by('path')\
                                                .filter(**self.filter_dict)\
                                                .order_by('-date')
