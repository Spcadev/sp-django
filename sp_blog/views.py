from django.shortcuts import get_object_or_404
from django.views.generic.dates import YearArchiveView, MonthArchiveView, DayArchiveView
from django.views.generic import DetailView, ListView

from endless_pagination.views import AjaxListView
from taggit.models import Tag

from .models import BlogPage, BlogPageTag, Series


class EntryDetailView(DetailView):
    template_name = 'sp_blog/entry_detail.html'
    context_object_name = 'entry'

    def __init__(self):
        super(EntryDetailView, self).__init__()

    def get_queryset(self):
        self.object = BlogPage.objects.live().filter(
            date__year=int(self.kwargs['year']),
            date__month=int(self.kwargs['month']),
            date__day=int(self.kwargs['day']),
            slug=self.kwargs['slug']
        )
        return self.object


class EntriesView(AjaxListView):
    queryset = BlogPage.objects.live().order_by('-date')
    date_field = 'date'
    make_object_list = True
    context_object_name = 'entries'
    template_name = 'sp_blog/entry_list.html'
    page_template = 'sp_blog/endless_entries.html'


class EntriesYearView(YearArchiveView, AjaxListView):
    queryset = BlogPage.objects.live().order_by('-date')
    date_field = 'date'
    make_object_list = True
    context_object_name = 'entries'
    template_name = 'sp_blog/entry_list.html'
    page_template = 'sp_blog/endless_entries.html'


class EntriesMonthView(MonthArchiveView, AjaxListView):
    queryset = BlogPage.objects.live().order_by('-date')
    date_field = 'date'
    make_object_list = True
    context_object_name = 'entries'
    month_format = '%m'
    template_name = 'sp_blog/entry_list.html'
    page_template = 'sp_blog/endless_entries.html'


class EntriesDayView(DayArchiveView, AjaxListView):
    queryset = BlogPage.objects.live().order_by('-date')
    date_field = 'date'
    make_object_list = True
    context_object_name = 'entries'
    month_format = '%m'
    template_name = 'sp_blog/entry_list.html'
    page_template = 'sp_blog/endless_entries.html'


class SeriesListView(ListView):
    queryset = Series.objects.all().order_by('-date')
    context_object_name = 'collections'
    make_object_list = True
    template_name = 'sp_blog/series_list.html'
    page_template = 'sp_blog/endless_series.html'


class SeriesDetailView(DetailView):
    template_name = 'sp_blog/series_detail.html'
    context_object_name = 'series'

    def __init__(self):
        super(SeriesDetailView, self).__init__()

    def get_queryset(self):
        return Series.objects.filter(slug=self.kwargs['slug'])

    def get_context_data(self, **kwargs):
        context = super(SeriesDetailView, self).get_context_data(**kwargs)
        context['entries'] = BlogPage.objects.live()\
            .filter(series__slug=self.kwargs['slug'])\
            .order_by('date')
        return context


class TaggedEntriesView(ListView):
    template_name = 'sp_blog/tag_detail.html'
    context_object_name = 'tag'

    def __init__(self):
        super(TaggedEntriesView, self).__init__()

    def get_queryset(self):
        return get_object_or_404(Tag, slug=self.kwargs['slug'])

    def get_context_data(self, **kwargs):
        context = super(TaggedEntriesView, self).get_context_data(**kwargs)
        context['entries'] = BlogPage.objects.live().filter(tags__slug=self.kwargs['slug']) \
            .order_by('-date')

        return context
