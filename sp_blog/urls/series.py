from django.conf.urls import patterns, url

from ..views import SeriesDetailView, SeriesListView


urlpatterns = patterns('sp_blog.views',
                       url(r'^$',
                           SeriesListView.as_view(),
                           name='series_list'
                       ),
                       url(r'^(?P<slug>[-\w]+)/$',
                           SeriesDetailView.as_view(),
                           name='series_detail',
                       ),
)