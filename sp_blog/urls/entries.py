from django.conf.urls import patterns, url

from ..views import (
    #EntryDetailView,
    EntriesView,
    EntriesYearView,
    EntriesMonthView,
    EntriesDayView,
    TaggedEntriesView,
)

urlpatterns = patterns('sp_blog.views',
                       url(r'^$',
                           EntriesView.as_view(),
                           name='entries'),
                       url(r'^(?P<year>\d{4})/$',
                           EntriesYearView.as_view(),
                           name='entries_year'),
                       url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/$',
                           EntriesMonthView.as_view(),
                           name='entries_month'),
                       url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/$',
                           EntriesDayView.as_view(),
                           name='entries_day'),
                       #url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<slug>[-\w]+)/$',
                           #EntryDetailView.as_view(),
                           #name='entry_detail'),
                       url(r'^tags/(?P<slug>[-\w]+)/$',
                           TaggedEntriesView.as_view(),
                           name='tag_detail'),
)
