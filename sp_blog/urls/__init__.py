from django.conf.urls import patterns, include, url

from ..feeds import LatestEntriesFeed

urlpatterns = patterns('',
                       url(r'^',
                           include('sp_blog.urls.entries')
                       ),
                       url(r'^series/',
                           include('sp_blog.urls.series')
                       ),
                       url(r'^rss/',
                           LatestEntriesFeed()
                       ),
)