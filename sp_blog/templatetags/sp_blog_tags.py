from datetime import datetime

from django import template
from django.db.models import Count
from django.template.defaultfilters import stringfilter

from classytags.core import Options
from classytags.arguments import Argument, IntegerArgument
from classytags.helpers import AsTag
from taggit.models import Tag

from ..models import BlogPage, BlogPageTag, Series

register = template.Library()


@register.filter
@stringfilter
def trim(value):
    return value.strip()


class ListRecent(AsTag):
    options = Options(
        IntegerArgument('limit', default=5, resolve=True, required=True),
        'as',
        Argument('varname', resolve=False, required=False),
    )

    def get_value(self, context, limit):
        """
        Put a list of recently published blog posts into the template
        context. A tag title or slug, category title or slug or author's
        username can also be specified to filter the recent posts returned.

        Usage::

            {% ListRecent 5 as recent_posts %}
            {% ListRecent limit=5 tag="django" as recent_posts %}
            {% ListRecent 5 username=admin as recent_posts %}

        """
        return BlogPage.objects.live().order_by('-date')[:limit]

register.tag('ListRecent', ListRecent)


class ListArchive(AsTag):
    """
    Put a list of dates for blog posts into the template context.
    """
    options = Options(
        'as',
        Argument('varname', resolve=False, required=False),
    )

    def get_value(self, context):
        dates = BlogPage.objects.live().values_list("date", flat=True)
        date_dicts = [{"date": datetime(d.year, d.month, 1)} for d in dates]
        month_dicts = []
        for date_dict in date_dicts:
            if date_dict not in month_dicts:
                month_dicts.append(date_dict)
        for i, date_dict in enumerate(month_dicts):
            month_dicts[i]["entry_count"] = date_dicts.count(date_dict)
        return month_dicts

register.tag('ListArchive', ListArchive)


class ListSeries(AsTag):
    options = Options(
        'as',
        Argument('varname', resolve=False, required=False),
    )

    def get_value(self, context):
        series = Series.objects.live()
        return list(series.annotate(entry_count=Count("entries")))

register.tag('ListSeries', ListSeries)


class ListSeriesEntries(AsTag):
    options = Options(
        IntegerArgument('pk', resolve=True, required=True),
        'as',
        Argument('varname', resolve=False, required=False),
    )

    def get_value(self, context, pk):
        entries = BlogPage.objects.live().filter(series=pk).order_by('date')
        return list(entries)

register.tag('ListSeriesEntries', ListSeriesEntries)


class GetBlogTags(AsTag):
    options = Options(
        'as',
        Argument('varname', resolve=False, required=False),
    )

    def get_value(self, context):
        queryset = BlogPageTag.objects.filter(content_object__live=True)
        tag_ids = queryset.values_list('id', flat=True)
        queryset = Tag.objects.filter(sp_blog_blogpagetag_items__id__in=tag_ids)
        return queryset.annotate(tag_count=Count('name')).order_by('-tag_count')

register.tag('GetBlogTags', GetBlogTags)


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)