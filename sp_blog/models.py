import datetime

from django.db import models

from wagtail.wagtailcore import blocks
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index

from modelcluster.fields import ParentalKey
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

from .blocks import ChecklistBlock, SnippetBlock, InfoBlock, WarningBlock, DangerBlock, MarkdownBlock
from .fields import LinkFields
from .managers import LiveManager


class RelatedLink(LinkFields):
    title = models.CharField(max_length=255, help_text="Link title")

    panels = [
        FieldPanel('title'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

    class Meta:
        abstract = True


class BlogIndexPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_blog.BlogIndexPage', related_name='related_links')


class BlogIndexPage(Page):
    description = RichTextField(blank=True)

    search_fields = Page.search_fields + (
        index.SearchField('description'),
    )

    @property
    def entries(self):
        # Get list of live blog pages that are descendants of this page
        entries = BlogPage.objects.live().descendant_of(self)
        entries = entries.order_by('-date')
        return entries

    def get_context(self, request):
        entries = self.entries
        context = super(BlogIndexPage, self).get_context(request)
        context['entries'] = entries
        return context

BlogIndexPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    InlinePanel(BlogIndexPage, 'related_links', label="Related links"),
]

BlogIndexPage.promote_panels = Page.promote_panels


class BlogPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_blog.BlogPage', related_name='related_links')


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey('sp_blog.BlogPage', related_name='tagged_items')


class BlogPage(Page):
    body = StreamField([
        ('heading', blocks.CharBlock(max_length=255)),
        ('entry', blocks.RichTextBlock()),
        ('markdown', MarkdownBlock()),
        ('snippet', SnippetBlock()),
        ('image', ImageChooserBlock()),
        ('list', blocks.ListBlock(ChecklistBlock())),
        ('info', InfoBlock()),
        ('warning', WarningBlock()),
        ('danger', DangerBlock()),
    ])
    comments_enabled = models.BooleanField(default=True)
    date = models.DateField("Post date")
    description = RichTextField(blank=True)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    search_fields = Page.search_fields + (
        index.SearchField('body'),
        index.SearchField('description'),
    )
    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)

    live = LiveManager()

    class Meta:
        verbose_name_plural = 'Blog Entries'
        ordering = ['-date']

    def __str__(self):
        return '%s' % self.title

    @property
    def blog_index(self):
        # Find closest ancestor which is a blog index
        return self.get_ancestors().type(BlogIndexPage).last()

    def get_absolute_url(self):
        return ('blog:entry_detail', (), {
            'year': self.date.strftime("%Y"),
            'month': self.date.strftime("%m").lower(),
            'day': self.date.strftime("%d"),
            'slug': self.slug})

    def get_next_entry(self, series_pk):
        filter_dict = dict(
            series=series_pk,
            path__gte=self.path,
            live=True,
            date__lte=datetime.datetime.now,)
        return self.get_next_by_date(**filter_dict)

    def get_previous_entry(self, series_pk):
        filter_dict = dict(
            series=series_pk,
            path__lte=self.path,
            live=True,
            date__lte=datetime.datetime.now,)
        return self.get_previous_by_date(**filter_dict)

    get_absolute_url = models.permalink(get_absolute_url)


BlogPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    StreamFieldPanel('body'),
    FieldPanel('date'),
]

BlogPage.promote_panels = Page.promote_panels + [
    FieldPanel('tags'),
    InlinePanel(BlogPage, 'related_links', label="Related links"),
    ImageChooserPanel('feed_image'),
    FieldPanel('comments_enabled'),
]


class Series(Page):
    comments_enabled = models.BooleanField(default=True)
    date = models.DateField("Post date")
    description = RichTextField(blank=True)
    entries = models.ManyToManyField(
        BlogPage,
        blank=True,
        help_text='Which entries are a part of this series?.')
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    search_fields = Page.search_fields + (
        index.SearchField('description'),
    )

    class Meta:
        ordering = ['title']
        verbose_name_plural = 'Series'

    def __str__(self):
        return '%s' % self.title

    def get_absolute_url(self):
        return '/blog/series/%s/' % self.slug

    @property
    def get_entries(self):
        entries = BlogPage.objects.live().descendant_of(self).order_by('-date')
        return entries


Series.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    FieldPanel('entries'),
    FieldPanel('date'),
]

Series.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
    FieldPanel('comments_enabled'),
]
