from django.contrib.syndication.views import Feed

from .models import BlogPage


class LatestEntriesFeed(Feed):
    title = 'SP.ca Blog Entries'
    link = '/latest/'
    description = 'Latest blog entries at Shawn Parsons.ca'

    def __init__(self):
        super(LatestEntriesFeed, self).__init__()

    def items(self):
        return BlogPage.objects.live().order_by('date')[:5]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.excerpt

    def item_link(self, item):
        return item.get_absolute_url()
