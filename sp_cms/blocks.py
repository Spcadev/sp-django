from wagtail.wagtailcore import blocks
from wagtail.wagtailimages.blocks import ImageChooserBlock

class SnippetBlock(blocks.StructBlock):
    """
    SnippetBlock wraps code in a styled frame for the blog.
    """
    code = blocks.TextBlock(
        required=True,
        help_text='Enter the code snippet here.'
    )
    file = blocks.CharBlock(
        required=False,
        help_text='If this code belongs in a specific file (eg: urls.py) enter that file name here.'
    )
    code_type = blocks.CharBlock(
        required=False,
        help_text='Enter the type of code (eg: Python) to help with syntax colouring.'
    )
    source_url = blocks.URLBlock(
        required=False,
        min_length=0,
        max_length=2048,
        help_text='If this code belongs to someone else, please link a source url.'
    )

    class Meta:
        template = 'sp_cms/blocks/code_snippet.html'
        icon = 'code'


class ChecklistBlock(blocks.StructBlock):
    """
    ChecklistBlock implements a Fontawesome based checklist for the blog.
    """
    item = blocks.CharBlock(
        required=True,
        help_text='Enter a list item.'
    )
    checked = blocks.BooleanBlock(
        required=False,
        help_text='Has this item been completed?'
    )

    class Meta:
        template = 'sp_cms/blocks/check_list.html'
        icon = 'tick'


class HomeMissionBlock(blocks.StructBlock):
    mission = blocks.RichTextBlock(
        required=True,
        help_text='The Mission Statement'
    )
    disclaimer = blocks.RichTextBlock(
        required=False,
        help_text='Fair Warning'
    )

    class Meta:
        template = 'sp_cms/blocks/home_mission.html'


class HomeAboutBlock(blocks.StructBlock):
    who_i_am = blocks.RichTextBlock()
    who_i_am_img = ImageChooserBlock()
    who_you_are = blocks.RichTextBlock()
    who_you_are_img = ImageChooserBlock()

    class Meta:
        template = 'sp_cms/blocks/home_about.html'


class PhaseBlock(blocks.StructBlock):
    icon = blocks.CharBlock(
        required=True,
        help_text='Font-Awesome icon CSS class.'
    )
    title = blocks.CharBlock(
        required=True,
        help_text='The phase title.'
    )
    tag_line = blocks.CharBlock()
    description = blocks.RichTextBlock()

    class Meta:
        template = 'sp_cms/blocks/phase.html'


class HomePhasesBlock(blocks.StructBlock):
    description = blocks.RichTextBlock()
    phase_one = PhaseBlock()
    phase_two = PhaseBlock()
    phase_three = PhaseBlock()

    class Meta:
        template = 'sp_cms/blocks/home_phases.html'


class MeSkillBlock(blocks.StructBlock):
    """
    MeSkillBlock wraps skills in a format improved by javascript progress bars.
    """
    name = blocks.CharBlock(
        required=True,
        max_length=255,
        help_text='The name of the skill.'
    )
    percent = blocks.CharBlock(
        min_length=1,
        max_length=2,
    )
    description = blocks.TextBlock()

    class Meta:
        template = 'sp_cms/blocks/skill.html'


class PhaseOneMenuBlock(blocks.StructBlock):
    title = blocks.CharBlock(
        required=True,
        max_length=255,
        help_text='App menu item for Phase 1'
    )

    class Meta:
        template = 'sp_cms/blocks/pone_menu.html'


class PhaseOneListBlock(blocks.StructBlock):
    title = blocks.CharBlock(
        required=True,
        max_length=255,
        help_text='App menu item for Phase 1'
    )
    description = blocks.RichTextBlock(
        required=False,
        help_text='Description for the application.'
    )
    requirements = blocks.ListBlock(ChecklistBlock())
    app_url = blocks.URLBlock(
        required=False,
        max_length=2048
    )
    github_url = blocks.URLBlock(
        required=False,
        max_length=2048
    )

    class Meta:
        template = 'sp_cms/blocks/pone_list.html'


class ImageContentBlock(blocks.StructBlock):
    title = blocks.CharBlock(
        required=True,
        max_length=32,
    )
    image = ImageChooserBlock()
    caption = blocks.CharBlock(
        required=False,
    )
    description = blocks.RichTextBlock(
        required=False,
    )
    content_url = blocks.URLBlock(
        required=False,
        max_length=2048
    )

    class Meta:
        template = 'sp_cms/blocks/image_content.html'


class SourcesBlock(blocks.StructBlock):
    """
    SouresBlock generates a definition list for content sources for the blog.
    """
    title = blocks.CharBlock(
        required=True,
        max_length=32,
    )
    description = blocks.RichTextBlock(
        required=False,
    )
    source_url = blocks.URLBlock(
        required=True,
        max_length=2048
    )

    class Meta:
        template = 'sp_cms/blocks/sources.html'