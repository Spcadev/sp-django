from django.db import models

from wagtail.wagtailcore import blocks
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailforms.models import AbstractEmailForm, AbstractFormField
from wagtail.wagtailsearch import index

from modelcluster.fields import ParentalKey

from .blocks import ChecklistBlock, SnippetBlock, HomeAboutBlock, HomeMissionBlock, HomePhasesBlock, MeSkillBlock, \
    PhaseOneMenuBlock, PhaseOneListBlock, ImageContentBlock, SourcesBlock
from .fields import ContactFields, LinkFields, QuoteFields


class CarouselItem(LinkFields):
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    embed_url = models.URLField("Embed URL", blank=True)
    caption = models.CharField(max_length=255, blank=True)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('embed_url'),
        FieldPanel('caption'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

    class Meta:
        abstract = True


class RelatedLink(LinkFields):
    title = models.CharField(max_length=255, help_text="Link title")

    panels = [
        FieldPanel('title'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

    class Meta:
        abstract = True


class HomePageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('sp_cms.HomePage', related_name='carousel_items')


class HomePageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_cms.HomePage', related_name='related_links')


class HomePage(Page):
    body = StreamField([
        ('mission', HomeMissionBlock()),
        ('about', HomeAboutBlock()),
        ('phases', HomePhasesBlock()),
    ])

    search_fields = Page.search_fields + (
        index.SearchField('body'),
    )

    class Meta:
        verbose_name = "Homepage"

HomePage.content_panels = [
    FieldPanel('title', classname="full title"),
    StreamFieldPanel('body'),
    InlinePanel(HomePage, 'carousel_items', label="Carousel items"),
    InlinePanel(HomePage, 'related_links', label="Related links"),
]

HomePage.promote_panels = Page.promote_panels


class StandardIndexPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_cms.StandardIndexPage', related_name='related_links')


class StandardIndexPage(Page):
    description = RichTextField(blank=True)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = Page.search_fields + (
        index.SearchField('description'),
    )

StandardIndexPage.content_panels = [
    FieldPanel('title', classname="full title"),
    InlinePanel(StandardIndexPage, 'related_links', label="Related links"),
]

StandardIndexPage.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
]


class StandardPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('sp_cms.StandardPage', related_name='carousel_items')


class StandardPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_cms.StandardPage', related_name='related_links')


class StandardPage(Page):
    description = RichTextField(blank=True)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    body = StreamField([
        ('heading', blocks.CharBlock(max_length=255)),
        ('entry', blocks.RichTextBlock()),
        ('snippet', SnippetBlock()),
        ('image', ImageChooserBlock()),
        ('list', blocks.ListBlock(ChecklistBlock()))
    ])

    search_fields = Page.search_fields + (
        index.SearchField('description'),
        index.SearchField('body'),
    )

StandardPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    InlinePanel(StandardPage, 'carousel_items', label="Carousel items"),
    StreamFieldPanel('body'),
    InlinePanel(StandardPage, 'related_links', label="Related links"),
]

StandardPage.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
]


class AboutPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_cms.AboutPage', related_name='related_links')


class AboutPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('sp_cms.AboutPage', related_name='carousel_items')


class AboutPage(Page):
    description = RichTextField(blank=True)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    body = StreamField([
        ('heading', blocks.CharBlock(max_length=255)),
        ('entry', blocks.RichTextBlock()),
        ('snippet', SnippetBlock()),
        ('image', ImageChooserBlock()),
        ('list', blocks.ListBlock(ChecklistBlock())),
        ('image_content', blocks.ListBlock(ImageContentBlock())),
        ('sources', blocks.ListBlock(SourcesBlock())),
    ])

    search_fields = Page.search_fields + (
        index.SearchField('description'),
        index.SearchField('body'),
    )

AboutPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    InlinePanel(AboutPage, 'carousel_items', label="Carousel items"),
    StreamFieldPanel('body'),
    InlinePanel(AboutPage, 'related_links', label="Related links"),
]

AboutPage.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
]


class PersonPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_cms.PersonPage', related_name='related_links')


class PersonPage(Page, ContactFields, QuoteFields):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    description = RichTextField(blank=True)
    biography = RichTextField(blank=True)
    body = StreamField([
        ('heading', blocks.CharBlock(max_length=255)),
        ('entry', blocks.RichTextBlock()),
        ('image', ImageChooserBlock()),
        ('list', blocks.ListBlock(ChecklistBlock())),
        ('skills', blocks.ListBlock(MeSkillBlock())),
    ], blank=True)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = Page.search_fields + (
        index.SearchField('first_name'),
        index.SearchField('last_name'),
        index.SearchField('description'),
        index.SearchField('biography'),
        index.SearchField('body'),
    )

PersonPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('first_name'),
    FieldPanel('last_name'),
    FieldPanel('description', classname="full"),
    FieldPanel('biography', classname="full"),
    ImageChooserPanel('image'),
    MultiFieldPanel(ContactFields.panels, "Contact"),
    MultiFieldPanel(QuoteFields.panels, "Quote"),
    StreamFieldPanel('body'),
]

PersonPage.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
    InlinePanel(PersonPage, 'related_links', label="Related links"),
]


class ContactPage(Page, ContactFields):
    body = RichTextField(blank=True)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = Page.search_fields + (
        index.SearchField('body'),
    )

ContactPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('body', classname="full"),
    MultiFieldPanel(ContactFields.panels, "Contact"),
]

ContactPage.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
]


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', related_name='form_fields')


class FormPage(AbstractEmailForm):
    description = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

FormPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    InlinePanel(FormPage, 'form_fields', label="Form fields"),
    FieldPanel('thank_you_text', classname="full"),
    MultiFieldPanel([
        FieldPanel('to_address', classname="full"),
        FieldPanel('from_address', classname="full"),
        FieldPanel('subject', classname="full"),
    ], "Email")
]


class PhaseOnePageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_cms.PhaseOnePage', related_name='related_links')


class PhaseOnePage(Page):
    description = RichTextField(blank=True)
    body = StreamField([
        ('heading', blocks.CharBlock(max_length=255)),
        ('entry', blocks.RichTextBlock()),
        ('snippet', SnippetBlock()),
        ('image', ImageChooserBlock()),
        ('list', blocks.ListBlock(ChecklistBlock())),
        ('menu', blocks.ListBlock(PhaseOneMenuBlock())),
        ('apps', blocks.ListBlock(PhaseOneListBlock())),
    ])
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = Page.search_fields + (
        index.SearchField('description'),
        index.SearchField('body'),
    )

PhaseOnePage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    StreamFieldPanel('body'),
]

PhaseOnePage.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
    InlinePanel(PhaseOnePage, 'related_links', label="Related links"),
]


class PhaseTwoPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_cms.PhaseTwoPage', related_name='related_links')


class PhaseTwoPage(Page):
    description = RichTextField(blank=True)
    body = StreamField([
        ('heading', blocks.CharBlock(max_length=255)),
        ('entry', blocks.RichTextBlock()),
        ('snippet', SnippetBlock()),
        ('image', ImageChooserBlock()),
        ('list', blocks.ListBlock(ChecklistBlock())),
    ])
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = Page.search_fields + (
        index.SearchField('description'),
        index.SearchField('body'),
    )

PhaseTwoPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    StreamFieldPanel('body'),
]

PhaseTwoPage.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
    InlinePanel(PhaseTwoPage, 'related_links', label="Related links"),
]


class PhaseThreePageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('sp_cms.PhaseThreePage', related_name='related_links')


class PhaseThreePage(Page):
    description = RichTextField(blank=True)
    body = StreamField([
        ('heading', blocks.CharBlock(max_length=255)),
        ('entry', blocks.RichTextBlock()),
        ('snippet', SnippetBlock()),
        ('image', ImageChooserBlock()),
        ('list', blocks.ListBlock(ChecklistBlock())),
    ])
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = Page.search_fields + (
        index.SearchField('description'),
        index.SearchField('body'),
    )

PhaseThreePage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    StreamFieldPanel('body'),
]

PhaseThreePage.promote_panels = Page.promote_panels + [
    ImageChooserPanel('feed_image'),
    InlinePanel(PhaseThreePage, 'related_links', label="Related links"),
]