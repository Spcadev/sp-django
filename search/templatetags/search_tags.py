from django import template
from django.utils.safestring import mark_safe

from classytags.core import Options
from classytags.arguments import Argument, StringArgument
from classytags.helpers import AsTag

import bleach  # Used for HTML sanitizing
import nltk.data  # Natrual Language Took Kit
import re

register = template.Library()

@register.filter(name='highlight')
def highlight(text, filter):
    pattern = re.compile(r"(?P<filter>%s)" % filter, re.IGNORECASE)
    return mark_safe(
        re.sub(
            pattern, r'<strong class="text-primary">\g<filter></strong>', str(text)))


@register.filter(name='to_class_name')
def to_class_name(value):
    return value.__class__.__name__


class ParseBlocks(AsTag):
    options = Options(
        StringArgument('needle', resolve=True, required=True),
        'in',
        Argument('haystack', resolve=True, required=True),
        'as',
        Argument('varname', resolve=False, required=False),
    )

    def get_value(self, context, needle, haystack):
        """
        Iterate all blocks in a stream block, strip most HTML and parse content into sentences.  From there, find needle
        and extract the sentence it resides in, plus surrounding sentences for context.  Will do the same for lists
        but will leave code snippets mostly intact.

        Usage::
            The following will search for 'Foo' in all blocks within the body stream block and return a dictionary of
            lists named 'blocks' with the keys 'excerpts', 'snippets' and 'list_excerpts':

            {% ParseBlocks 'Foo' in object.body as blocks %}

            {% for block in blocks.excerpts %}
                <p>{{ block|highlight:'Foo' }}</p>
            {% endfor %}
        """

        # Home block white list, because it's special like that?
        home_blocks = (
            'mission', 'about', 'phases',
            'who_i_am', 'who_you_are', 'who_i_am_not'
            'phase_one', 'phase_two', 'phase_three',
        )

        # Result containers:
        results = {}
        excerpts = []
        excerpt = []
        snippets = []
        list_excerpts = []
        list_excerpt = []

        # Initiate NLTK tokenizer:
        tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

        # ToDo: Clean up redundancy and remove need for 'home_blocks'.
        # Revert haystack to essentially plain text, split into sentences:
        for block in haystack:
            if block.block_type in home_blocks:
                clean_item = bleach.clean(block, tags=[], strip=True, strip_comments=True)
                sentences = tokenizer.tokenize(clean_item.strip())
                # Find position of needle and grab surrounding sentences, if they exist:
                for key, sentence in enumerate(sentences):
                    if needle.upper() in sentence.upper():
                        if key > 0:
                            length = len(sentences[key - 1]) - 1
                            if length > 8:
                                excerpt.append('...' + sentences[key - 1][length - 8:length])
                            else:
                                excerpt.append(sentences[key - 1])
                        excerpt.append(sentence)
                        if key < len(sentences) - 1:
                            length = len(sentences[key + 1]) - 1
                            if length > 8:
                                excerpt.append(sentences[key + 1][0:8] + '...')
                            else:
                                excerpt.append(sentences[key + 1])
                excerpts.append('\n\n'.join(excerpt))
                excerpt = []  # Clear for next iteration.
            elif block.block_type == 'list':
                for key, value in enumerate(block.value):
                    clean_value = str(bleach.clean(value, tags=[], strip=True, strip_comments=True))
                    if needle.upper() in clean_value.upper():
                        if key > 0:
                            if key - 1 > 0:
                                list_excerpt.append(' ... ')
                            list_excerpt.append(block.value[key - 1])
                        list_excerpt.append(value)
                        if key < len(block.value) - 1:
                            list_excerpt.append(block.value[key + 1])
                            if key < len(block.value) - 2:
                                list_excerpt.append(' ... ')
                list_excerpts.append(list_excerpt)
                list_excerpt = []  # Clear for next iteration.
            elif block.block_type == 'snippet':
                snippets.append(block)
            elif block.block_type == 'entry':
                clean_item = bleach.clean(block, tags=[], strip=True, strip_comments=True)
                sentences = tokenizer.tokenize(clean_item.strip())
                # Find position of needle and grab surrounding sentences, if they exist:
                for key, sentence in enumerate(sentences):
                    if needle.upper() in sentence.upper():
                        if key > 0:
                            length = len(sentences[key - 1]) - 1
                            if length > 8:
                                excerpt.append('...' + sentences[key - 1][length - 8:length])
                            else:
                                excerpt.append(sentences[key - 1])
                        excerpt.append(sentence)
                        if key < len(sentences) - 1:
                            length = len(sentences[key + 1]) - 1
                            if length > 8:
                                excerpt.append(sentences[key + 1][0:8] + '...')
                            else:
                                excerpt.append(sentences[key + 1])
                excerpts.append('\n\n'.join(excerpt))
                excerpt = []  # Clear for next iteration.
            elif block.block_type == 'image':
                pass
            else:
                for item in block.value:
                    clean_item = bleach.clean(item, tags=[], strip=True, strip_comments=True)
                    sentences = tokenizer.tokenize(clean_item.strip())
                    # Find position of needle and grab surrounding sentences, if they exist:
                    for key, sentence in enumerate(sentences):
                        if key > 0:
                            length = len(sentences[key - 1]) - 1
                            if length > 8:
                                excerpt.append('...' + sentences[key - 1][length - 8:length])
                            else:
                                excerpt.append(sentences[key - 1])
                        excerpt.append(sentence)
                        if key < len(sentences) - 1:
                            length = len(sentences[key + 1]) - 1
                            if length > 8:
                                excerpt.append(sentences[key + 1][0:8] + '...')
                            else:
                                excerpt.append(sentences[key + 1])
                excerpts.append('\n\n'.join(excerpt))
                excerpt = []  # Clear for next iteration.

        results['snippets'] = snippets
        results['list_excerpts'] = list_excerpts
        results['excerpts'] = excerpts
        return results

register.tag('ParseBlocks', ParseBlocks)


class CountAll(AsTag):
    options = Options(
        StringArgument('needle', resolve=True, required=True),
        'in',
        Argument('haystack', resolve=True, required=True),
        'as',
        Argument('varname', resolve=False, required=False),
    )

    def get_value(self, context, needle, haystack):
        """
        Count all instances of a term (needle) in a given body (haystack).

        Usage::

            {% CountAll 'foo' in 'foo bar' %}  # Returns 1
            {% CountAll 'foo' in 'foo bar' as count %}  # Stores 1 in variable 'count'
            {% CountAll search_term in text_body as count %}  # Uses variables and stores result in 'count'

        """
        return haystack.__str__().upper().count(needle.upper())

register.tag('CountAll', CountAll)
