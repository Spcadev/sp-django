# DO NOT USE THIS #

** This repo is *intentionally* incomplete and is here for historical reasons only.**

This started out as an experiment with Python and Django for a personal website and was run for about 10 months before I took it down.

This used Django 1.8 with Wagtail 1.0 for CMS and Blog duties.  For an end-user friendly dynamic content management system, I would be hard pressed to name a better alternative to Wagtail for a Python driven website, and strongly recommend it.

The site also used Django-Rest-Framework, or at least had a working implementation for the API, but there was never a front end to go with it.  Much of that code has been ripped out, but you can still see some cruft in the URLS and sp_users sections.

There are a number of custom blocks and fields set up to provide extra functionality for the steam fields in the blog, as well as a few of the static pages for the site.

I took a (failed) stab at improving the search functionality that comes baked into Wagtail, and may one day improve upon that (if they haven't already).

Some of this is documented, and some of it is not.  There are no unit tests.