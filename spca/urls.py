import os

from django.conf.urls import include, url, patterns
from django.conf import settings
from django.contrib import admin

from rest_framework.authtoken import views
from rest_framework.routers import DefaultRouter
from wagtail.contrib.wagtailsitemaps.views import sitemap
from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtaildocs import urls as wagtaildocs_urls
from wagtail.wagtailcore import urls as wagtail_urls

from sp_users.views import UserViewSet, GroupViewSet

admin.autodiscover()

if os.environ.get('ADMIN_URL_PATH'):
    admin_url_path = os.environ.get('ADMIN_URL_PATH')
else:
    admin_url_path = settings.ADMIN_URL_PATH

if os.environ.get('WAGTAIL_ADMIN_URL_PATH'):
    wagtail_admin_url_path = os.environ.get('WAGTAIL_ADMIN_URL_PATH')
else:
    wagtail_admin_url_path = settings.ADMIN_URL_PATH

# Django Rest Framework route registration:
router = DefaultRouter()
#router.register(r'todo_users', TodoUserViewSet)
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)

#  Base website.  Urls must come before the Wagtail catch-all url:
urlpatterns = patterns('',
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    url(r'^{}/admin/'.format(admin_url_path), include(admin.site.urls)),
    url(r'^{}/wagtail/'.format(wagtail_admin_url_path), include(wagtailadmin_urls)),
    url(r'^search/', 'search.views.search', name='search'),
    url(r'^documents/', include(wagtaildocs_urls)),
    url(r'^blog/', include('sp_blog.urls', app_name='blog', namespace='sp_blog')),
)

# Frontend API.  Must come before the Wagtail catch-all url:
urlpatterns += patterns('',
    url(r'^api/', include(router.urls)),  # App_name & namespace cause errors in DRF!
    url(r'^api/docs/', include('rest_framework_swagger.urls')),
    url(r'^apps/', include('sp_apps.urls', app_name='apps', namespace='sp_apps')),
    url(r'^api/token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),  # For Django-JWT-Auth.
    url(r'^api/token-refresh/', 'rest_framework_jwt.views.refresh_jwt_token'),  # For Django-JWT-Auth.
)

# Django-debug-toolbar must come before the Wagtail catch-all url:
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )

# Wagtail catch-all serving mechanism + sitemap.xml & robots.txt generator:
urlpatterns += patterns('',
    url(r'^robots\.txt$', include('robots.urls')),
    url(r'^sitemap\.xml$', sitemap),
    url(r'', include(wagtail_urls)),
)

# Static & Media setup:
if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)