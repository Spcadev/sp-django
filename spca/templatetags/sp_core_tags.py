import datetime

from django import template

from classytags.core import Options
from classytags.arguments import Argument
from classytags.helpers import AsTag

register = template.Library()


class GetCurrentDate(AsTag):
    options = Options(
        'as',
        Argument('varname', resolve=False, required=False),
    )

    def get_value(self, context):
        """
        Return the current date to help build the main menu blog options.

        Usage::
            {% GetCurrentDate as today %}
        """
        return datetime.datetime.now()

register.tag('GetCurrentDate', GetCurrentDate)