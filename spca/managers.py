import datetime
from django.db import models


class LiveManager(models.Manager):
    def get_queryset(self):
        return super(LiveManager, self).get_queryset()\
            .exclude(pub_date__gt=datetime.datetime.now)\
            .exclude(expires__lte=datetime.datetime.now)\
            .filter(status='public')


class PrivateManager(models.Manager):
    def get_queryset(self):
        return super(PrivateManager, self).get_queryset() \
            .exclude(pub_date__gt=datetime.datetime.now) \
            .exclude(expires__lte=datetime.datetime.now) \
            .filter(status='private')


class FeaturedManager(models.Manager):
    def get_queryset(self):
        return super(FeaturedManager, self).get_queryset()\
            .exclude(pub_date__gt=datetime.datetime.now)\
            .exclude(expires__lte=datetime.datetime.now)\
            .filter(
                status='public',
                featured_until__gt=datetime.datetime.now)


class ParentChildManager(models.Manager):
    def get_roots(self):
        return self.get_queryset().filter(parent__isnull=True)