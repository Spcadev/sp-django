from django.contrib.contenttypes.models import ContentType
from django.db import models, IntegrityError, transaction
from django.template.defaultfilters import slugify
from django.utils import timezone

from model_utils.fields import AutoCreatedField, AutoLastModifiedField
from markdown import markdown

from .managers import ParentChildManager


class SelfAwareModel(models.Model):
    def get_content_type(self):
        return ContentType.objects.get_for_model(self)

    def get_content_type_id(self):
        return self.get_content_type().pk

    def get_app_label(self):
        return self.get_content_type().app_label

    def get_model_name(self):
        return self.get_content_type().model

    class Meta:
        abstract = True


class TitleSlugModel(models.Model):
    """
    An abstract base class model that provides a title & slug combo that is used very often.
    """
    title = models.CharField(
        blank=False,
        null=False,
        max_length=128,
        help_text='Maximum 128 characters. '
                  'Must be unique.')
    slug = models.SlugField(
        blank=False,
        null=False,
        help_text='Suggested value automatically generated from title.')

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """
        Based on the Tag save() method in django-taggit, this method simply
        stores a slugified version of the title, ensuring that the unique
        constraint is observed
        """
        self.slug = slug = slugify(self.title)
        i = 0
        while True:
            try:
                savepoint = transaction.savepoint()
                res = super(TitleSlugModel, self).save(*args, **kwargs)
                transaction.savepoint_commit(savepoint)
                return res
            except IntegrityError:
                transaction.savepoint_rollback(savepoint)
                i += 1
                self.slug = '%s_%d' % (slug, i)


class UniqueTitleSlugModel(models.Model):
    """
    An abstract base class model that provides a title & slug combo that is used very often.
    """
    title = models.CharField(
        unique=True,
        blank=False,
        null=False,
        max_length=128,
        help_text='Maximum 128 characters. '
                  'Must be unique.')
    slug = models.SlugField(
        unique=True,
        blank=False,
        null=False,
        help_text='Suggested value automatically generated from title. '
                  'Must be unique.')

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """
        Based on the Tag save() method in django-taggit, this method simply
        stores a slugified version of the title, ensuring that the unique
        constraint is observed
        """
        self.slug = slug = slugify(self.title)
        i = 0
        while True:
            try:
                savepoint = transaction.savepoint()
                res = super(UniqueTitleSlugModel, self).save(*args, **kwargs)
                transaction.savepoint_commit(savepoint)
                return res
            except IntegrityError:
                transaction.savepoint_rollback(savepoint)
                i += 1
                self.slug = '%s_%d' % (slug, i)


class DescriptionModel(models.Model):
    """
    An abstract base class model that provides a title & slug combo that is used very often.
    """
    description = models.TextField(
        blank=False, )
    description_html = models.TextField(
        editable=False,
        null=True, )

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        if self.description:
            self.description_html = markdown(self.description)
        super(DescriptionModel, self).save(force_insert, force_update)


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self updating 'created' and 'modified' fields.
    """
    created = AutoCreatedField()
    modified = AutoLastModifiedField()
    pub_date = models.DateTimeField(
        default=timezone.now(),
        blank=False,
        null=True,
        help_text='When will this start to be shown on the website?')
    expires = models.DateTimeField(
        blank=True,
        null=True,
        help_text='When will this stop being shown on the website?')

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.pk:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(TimeStampedModel, self).save(*args, **kwargs)


class ParentChildModel(models.Model):
    """
    An abstract base class model that implements parent-child relantions and is descendant aware.
    """
    parent = models.ForeignKey(
        'self',
        blank=True,
        null=True)

    tree = ParentChildManager()

    def get_children(self):
        return self._default_manager.filter(parent=self)

    def get_descendants(self):
        descendants = set(self.get_children())
        for node in list(descendants):
            descendants.update(node.get_descsendants())
        return descendants

    class Meta:
        abstract = True
